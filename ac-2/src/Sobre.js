import React from 'react';
import { Link } from "react-router-dom";
import "./Sobre.css"

export default function Sobre (){
    return (
        <div className='Sobre'>
            <h1>Qual o motivo desta aplicação?</h1>
            <h3>
                Esta é uma aplicação criada para concluir a segunda Atividade Contínua da
                matéria de Frameworks Fullstack do curso de Sistemas da Informação da Faculdade
                Impacta de Tecnologia.
            </h3>
            <h1>Existe algum motivo a mais para isso?</h1>
            <h3>Não :)</h3>
            <h3>
                Os repositórios que uso para as aulas e prática até o momento são privados. Então
                a não ser que eu queira mostrar o que eu aprendi desde que fiz este repositório
                não haverá mudança. Mas pense pelo lado bom, se este repositório recebeu mais
                atualizações além da data de 18/04/2022 quer dizer que você está (ou pode estar) 
                vendo um site completamente diferente de quando eu o fiz pela primeira vez. Essa é
                a mágica da internet e da nuvem, você pode estar vendo coisas de anos atrás e ver
                como as coisas estavam para essa pessoa ou grupo de pessoas nessa época :D
            </h3>
            <Link to='/home'>retornar a página home</Link> 
        </div>
    );
}
