import React from 'react';
import {Link} from 'react-router-dom';
import "./Home.css"

export default function Home(){
    return (
        <div className='Home'>
            <h1 className='teste'>Bem vindo à Home da aplicação!</h1>
            <Link to="/sobre">Saiba mais sobre esta página.</Link>
        </div>
    );
}