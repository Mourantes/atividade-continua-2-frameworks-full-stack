import React from 'react';
import {Switch, Route, Link} from 'react-router-dom';
import Sobre from './Sobre';
import Home from './Home';
import "./App.css";

export default function App() {
  return (
    <>
    <header className='header'>
      <Link to='/home'>Home</Link>
      <Link to='/sobre'>Sobre</Link>
    </header>
    <main>
      <Switch>
        <Route path='/home' component= {Home}/>
        <Route path='/sobre' component= {Sobre}/>
      </Switch>
    </main></>
  );
}
